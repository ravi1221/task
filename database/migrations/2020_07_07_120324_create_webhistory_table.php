<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebhistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webhistory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->engine = "InnoDB";
            $table->ipAddress('ip_address'); 
            $table->integer('ip_id')->unsigned();           
            $table->text('url')->nullable();
            $table->date('date');          
            $table->timestamps();
        });
        Schema::table('webhistory', function (Blueprint $table) {
            $table->foreign('ip_id')->references('id')->on('employer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webhistory');
    }
}
