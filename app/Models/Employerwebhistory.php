<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Employeesmodel;

class Employerwebhistory extends Model
{
    //
    protected $table = "webhistory";
    protected $fillable = ["ip_address","url","date"];

    public function employe()
    {
        return $this->belongTo(Employeesmodel::class);
    }
    
}
