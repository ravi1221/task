<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Employerwebhistory;

class Employeesmodel extends Model
{
    use SoftDeletes;
    //
    protected $table = "employer";
    protected $date = ['deleted_at'];
    protected $fillable = [
        'emp_id', 'emp_name', 'ip_address'
    ];

    public function webhistory()
    {
        return $this->hasMany(Employerwebhistory::class,'ip_id')->select(['ip_id','url','ip_address']);
    }
}
