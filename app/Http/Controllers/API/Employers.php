<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employeesmodel;
use Illuminate\Support\Facades\Validator;
use App\Models\Employerwebhistory;
use Carbon\Carbon;


class Employers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), $this->profileRules());
        if($validate->fails()){
            $errors = $validate_setting->messages();
            return response()->json([                
                 'success' => 'error',
                 'message' => $errors,
             ],400);
        }
       $empModel =  new Employeesmodel;
       $empModel->emp_id = $request->emp_id;
       $empModel->emp_name = $request->emp_name;
       $empModel->ip_address = $request->ip_address;
       $empModel->save();
       
       return response()->json([
           'status' => true,
           'msg' => 'Data has been inserted succesfully'
       ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ip_address)
    {       
        try{
            $data =  Employeesmodel::where('ip_address',$ip_address)->firstOrFail();
            return response()->json([
                'success' => true,
                'data' => $data
            ],201);
        }catch (\Exception $e) {
            return response()->json([
                'message' => 'Something went wrong please try again!',
                'success' => false,
            ],400);
        }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ip_address)
    {
       $delete =  Employeesmodel::where('ip_address',$ip_address)->delete();
    }
    public static function profileRules (){
        return [
            'emp_id'     => 'required',
            'emp_name'   => 'required|max:50|min:1',            
            'ip_address' => 'required',
        ];
    }

    public function storeWeb(Request $request)
    {    
        $today = Carbon::today();          
       $ip_exist =  Employeesmodel::where('ip_address','=',$request->ip_address)->first();
       if($ip_exist != null){
       $empHistory =  new Employerwebhistory;
       $empHistory->ip_address = $request->ip_address;
       $empHistory->url = $request->url;
       $empHistory->date = $today->format('Y-m-d');
       $empHistory->ip_id = $ip_exist->id;
       $empHistory->save();
       return response()->json([
           'status' => true,
           'message' => 'Employe url details stored successfully'
       ],201);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Ip Address is not exists'
            ],201);
        }
    }

    public function webEmpHistory($ip)
    {
        try{
            $empDetails = Employeesmodel::with('webhistory')
            ->where('ip_address',$ip)
            ->get()->toArray();
            $link = [];
            $ip_add = [];
            foreach($empDetails[0]['webhistory'] as $url){
                array_push($link,$url['url']);
               
            }
            $emplyeser = [
                "emp_id" => $empDetails[0]['emp_id'],
                "emp_name" => $empDetails[0]['emp_name'],
                "ip" => $empDetails[0]['ip_address'],
    
            ];
            $historyDe = [
                "id" => $empDetails[0]['webhistory'][0]['ip_id'],
                "ip" => $empDetails[0]['webhistory'][0]['ip_address'],
                "urls" => $link
            ];
            // dd($emplyeser);
            // dd(json_encode($empDetails[0]['webhistory']));
            return response()->json([
                'employer' => $emplyeser,
                "employeewebhistory" => $historyDe
            ],200);
        }catch (\Exception $e) {
            return response()->json([
                'message' => 'IP Is not valid!',
                'success' => false,
            ],400);
        }
      
    }

    public function destoryMap($ip)
    {
        try{
            $groupDel =  Employeesmodel::where('ip_address',$ip)->firstOrFail();
            $groupDel->webhistory()->delete();
            return response()->json([
                "sataus" => true,
                "message" => "Data has been deleted"
            ],200);
        }catch (\Exception $e) {
            return response()->json([
                'message' => 'Something went wrong!',
                'success' => false,
            ],400);
        }
      
    }
}
