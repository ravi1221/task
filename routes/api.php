<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/create','API\Employers@store');
Route::get('/show/{id}','API\Employers@show');
Route::get('/delete/{id}','API\Employers@destroy');
Route::post('/storeweb','API\Employers@storeWeb');
Route::get('/history/{id}','API\Employers@webEmpHistory');
Route::get('/destoryMap/{id}','API\Employers@destoryMap');
